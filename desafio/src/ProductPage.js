import React, { Component } from 'react'

class ProductPage extends Component {
    render () {
        return (
            <div>
                <h1>Produto: {this.props.product.name}</h1>
                <h1>{this.props.product.productId}</h1>
                <h1>Tamanho: {this.props.product.dimensionsMap.tamanho[2]}</h1>
                <h1>{this.props.product.nome}</h1>
            </div>
        )
    }
}

export default ProductPage